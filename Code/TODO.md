#TODO list

**Get things up and running**
- Scaled loss function
  - Tensorflow (look up the functions again and use debugger!)
  - Fix up the loss function to have a 1 - (for more generability)

*TEST USING 1 INPUT ONLY for speed!*
- Changing from 3 outputs to 2 outputs
  ENSURE THAT THE SPLITS ARE CORRECT!

  - DermoKnet
    - Need to change the model file
    - Need to change the meta data to output 2 classes
    - Wrap in a python file

  - Recod Titans
    - Script to automate and you need a fixer script!
      - Step through everything slowly
      - Multiple files need to be changed!
    - Adjust global step size

After base models finished:
  - 1 single automation script in python to train each base model indivudally
  python <script> <model: derma/recod> <test/train>

