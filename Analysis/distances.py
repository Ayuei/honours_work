
from PIL import Image
import glob
from collections import defaultdict
import numpy as np
import progressbar
import os
import fnmatch
import dill as pickle

# Feature signal : differences between pairs of frames (optical flow?)
# Middle frames


def calculate_distance(i1, i2):
    return np.sum((i1-i2)**2)


BASE_DIR = "/run/user/470852/gvfs/smb-share:server=research-data.shared.sydney.edu.au,share=rds-01/PRJ-MelanomaCAD/WorkingCopy/"

candidates = ['No excision - benign/HRMC_38/5', 'No excision - benign/HRMC_112/1', 'No excision - benign/HRMC_112/14', 'No excision - benign/HRMC_13/7', 'No excision - benign/HRMC_80/5', 'No excision - benign/HRMC_39/1', 'No excision - benign/HRMC_39/2', 'No excision - benign/HRMC_91/2', 'No excision - benign/HRMC_3/15', 'No excision - benign/HRMC_3/7', 'No excision - benign/HRMC_97/6', 'No excision - benign/HRMC_32/3', 'No excision - benign/HRMC_22/9', 'No excision - benign/HRMC_22/4', 'No excision - benign/HRMC_22/6', 'No excision - benign/HRMC_94/7', 'No excision - benign/HRMC_94/10', 'No excision - benign/HRMC_94/9', 'No excision - benign/HRMC_94/3', 'No excision - benign/HRMC_94/4', 'No excision - benign/HRMC_94/6', 'No excision - benign/HRMC_94/2', 'No excision - benign/HRMC_34/7', 'No excision - benign/HRMC_34/4', 'No excision - benign/HRMC_34/6', 'No excision - benign/HRMC_73/1', 'No excision - benign/HRMC_73/17', 'No excision - benign/HRMC_73/4', 'No excision - benign/HRMC_85/1', 'No excision - benign/HRMC_85/2', 'No excision - benign/HRMC_10/2', 'No excision - benign/HRMC_24/4', 'No excision - benign/HRMC_41/1', 'No excision - benign/HRMC_1/9', 'No excision - benign/HRMC_29/7', 'No excision - benign/HRMC_29/6', 'No excision - benign/HRMC_46/1', 'No excision - benign/HRMC_69/10', 'No excision - benign/HRMC_71/2', 'No excision - benign/HRMC_109/1', 'No excision - benign/HRMC_109/2', 'No excision - benign/HRMC_49/10', 'No excision - benign/HRMC_49/14', 'No excision - benign/HRMC_89/3', 'No excision - benign/HRMC_16/9', 'No excision - benign/HRMC_67/10', 'No excision - benign/HRMC_74/5', 'No excision - benign/HRMC_104/2', 'No excision - benign/HRMC_56/2', 'No excision - benign/HRMC_87/1']

d = defaultdict(lambda: [])

bar = progressbar.ProgressBar(max_value=len(candidates))
for bar_num, key in enumerate(candidates):
    # glob sort first last

    gather = glob.glob(BASE_DIR + key + "/*.jpg")
    gather.sort()

    for i in range(len(gather) - 1):
        d[key].append(calculate_distance(np.array(Image.open(gather[i])), np.array(Image.open(gather[i + 1]))))

    bar.update(bar_num)

pickle.dump(d, open("distances_dump_candidates.pkl", "wb+"))

BASE_DIR = "/run/user/470852/gvfs/smb-share:server=research-data.shared.sydney.edu.au,share=rds-01/PRJ-MelanomaCAD/WorkingCopy/Copy/"

total_image_distance = 0
total_images = 0



BASE_DIR = "/run/user/470852/gvfs/smb-share:server=research-data.shared.sydney.edu.au,share=rds-01/PRJ-MelanomaCAD/WorkingCopy/Copy/"

def get_folders(DIR):
    for root, dirnames, filenames in os.walk(DIR):
        for filename in fnmatch.filter(filenames, '*.jpg'):
            yield os.path.join(root, filename)


print()

d_folders = defaultdict(lambda: [])

for image_path in get_folders(BASE_DIR):
    d_folders['/'.join(image_path.split('/')[:-1])].append(image_path)
bar = progressbar.ProgressBar(max_value=len(d_folders))
d_average = defaultdict(lambda: [])


for bar_num, key in enumerate(d_folders):

    gather = sorted(d_folders[key])

    for i in range(len(gather) - 1):
        d_average[key].append(calculate_distance(np.array(Image.open(gather[i])), np.array(Image.open(gather[i + 1]))))

    bar.update(bar_num)


pickle.dump(d_average, open("distances_dump_average.pkl", "wb+"))


d_first_last = {}


for bar_num, key in enumerate(d_folders):

    gather = sorted(d_folders[key])

    d_first_last[key] = (calculate_distance(np.array(Image.open(gather[0])), np.array(Image.open(gather[-1]))))

    bar.update(bar_num)


pickle.dump(d_average, open("distances_dump_first_last.pkl", "wb+"))