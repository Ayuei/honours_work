from abc import ABC, abstractmethod
import os
import importlib
import numpy as np
import subprocess
import shlex
import datetime
import sklearn.metrics as m

# TODO
# Use importlib for the subfolder stuffs.

def run_process(cmd, args):
    args = " "+" ".join(args)
    args = args.replace("//", "/")
    print("Args:", args)
    print("Cmd:", cmd)
    print("Whole command", cmd+args)
    os.system(cmd+args)


def metrics(preds, grd_trth):
    m.accuracy_score(grd_trth, preds)

    print()
    m.confusion_matrix(grd_trth, preds)


def write_to_file(f, data):
    for datum in data:
        f.write(datum)


class Model(ABC):
    def __init__(self, path, **kwargs):
        self.path = path

        for kwarg in kwargs:
            setattr(self, kwarg, kwargs[kwarg])

        if not os.path.exists(self.path+"/"+self.output_folder):
            os.makedirs(self.path+"/"+self.output_folder)

        self.output_folder = self.path+"/"+self.output_folder
    @abstractmethod
    def predict(self, *args, **kwargs):
        pass

    @abstractmethod
    def train(self, *args, **kwargs):
        pass


class dermoNET(Model):

    def __init__(self, path, **kwargs):
        self.output_folder = ""
        super().__init__(path, **kwargs)


    def _predicter(self, f):

        preds = []
        names = []

        with open(f) as data:
            lines = data.readlines()[1:]
            for line in lines:
                if len(line) > 2:
                    n, p, _ = line.split()
                    preds.append(p)
                    names.append(n)
                else:
                    print("Sanity Check in Dermo _predicter:", line)

        #import pickle

        #names_to_match = pickle.load("isbi2017-part3", "rb")

        #inv_map = {v: k for k, v in names_to_match}

        #for i, name in enumerate(names):
        #    names[i] = inv_map[name]

        return names, preds

    # TODO
    # Post-process results?
    # Also need to modify run_pipline main method etc
    # Also need to do batch testing/training
    def predict(self, batch=None):
        from mat_dermo_code import run_pipeline as mat_dermo_net
        names, preds = mat_dermo_net.main(batch,
                       self.output_folder)

        return self._predicter(self.output_folder+"/predictions.txt")

    # TODO
    # CALL THE already embedded files to do the weaksegmentation network and
    # also the network itself.
    # Perhaps don't even need to train both???
    def train(self):
        import mathlab.engine

        eng = matlab.engine.start_matlab()

        # Inputs
        # modelStructurePath, expDir, opts

        try:
            eng.train.tranDiagnosisNet("data/netDefs/completeNet.m", self.output_folder)
        except:
            print("Something bad happened in our diagnosis training at the matcovnet")
            raise ValueError

        print("Training done for DermoNet")

class RECOD(Model):

    def __init__(self, path, **kwargs):
        self.output_folder = ""
        super().__init__(path, **kwargs)

    def _predicter(self, f):

        labels = None
        preds = None

        with open(f, 'r') as data:
            for datum in data.readlines():
                labels, prob_m, _ = datum.split()
                preds.append(1 if float(prob_m) >= 0.50 else 0)

        return labels, preds

    # TODO
    # need to implement training and per-image input. (MOST LIKELY Never per
    # image, so batch should be implemented too.
    # Make a deploy file with just one line and meta data
    # And just another python file to make the necessary tfr records.
    # TODO
    # Put in-between prediction files into an output folder (use timestamp/output file)
    # Yeh this makes sense rather than naming the final prediction file!

    def _prepare_data(self, train_batch=None, test_batch=None, vali_batch=None):
        def seek(a, f):
            for l in f:
                if a in l.split()[0]:
                    return l

        if test_batch is None:
            print("No test data???")
            raise ValueError

        batches_to_be_converted = []

        with open(self.output_folder+"/train.temp", "w+") as train, \
             open(self.output_folder+"/test.temp", "w+") as test, \
             open(self.output_folder+"/valid.temp", "w+") as vali, \
             open("deploy_2017.txt", "r") as deploy:

            meta_data = deploy.readlines()

            if train_batch is not None:
                train_data = [seek(_file, meta_data) for _file in train_batch]
                write_to_file(train, train_data)
                batches_to_be_converted.append("TRAIN")

            if test_batch is not None:
                test_data = [seek(_file, meta_data) for _file in test_batch]
                write_to_file(test, test_data)
                batches_to_be_converted.append("TEST")

            if vali_batch is not None:
                vali_data = [seek(_file, meta_data) for _file in vali_batch]
                write_to_file(vali, vali_data)
                batches_to_be_converted.append("VALI")

        for key in batches_to_be_converted:

            meta_file = self.output_folder+"/"+key.lower()+".temp"
            args = [key.lower(), key, meta_file, self.output_folder+"/"]

            run_process("./prepare_training_set", args)

            #subprocess.call(shlex.split("./prepare_training_set"+args))

    def predict(self, batch=None):

        run_process('./isbi2017-part3/args.sh', [self.output_folder])

        #subprocess.call(shlex.split('./isbi2017-part3/args.sh '+self.output_folder))
        return self._predicter('isbi2017-part3/raw_predictions.txt')


    # TODO
    # Make training function with all or per-image option? Nah mostly likey is
    # always a batch
    def train(self, train_batch, test_batch, vali_batch=None):
        self._prepare_data(train_batch, test_batch, vali_batch)


        # TODO !!!!
        # Make sure to only do the transfer learning component on copied models!!!
        # Make sure they get their own respective folders

        args = [self.output_folder]
        run_process("./"+self.path+"train.sh", args)

        print("Training done for RECOD titans")


class Ensemble:
    def __init__(self, models, weights=None, weighting_function=None, **kwargs):

        self.models = models
        self.weights = np.array(weights) if weighting_function is None else None
        self.weighting_function = weighting_function if weights is None else None

        for model in models:
            if not isinstance(model, Model):
                print("Unknown datatype used for model, need Model objects")
                raise TypeError

        if weights is None and weighting_function is None:
            print("Need at least 1 weighting mechanism")
            raise ValueError

        for kwarg in kwargs:
            setattr(self, kwarg, kwargs[kwarg])

    def predict(self, batch=None):
        predictions = []
        names = []

        for model in self.models:
            print("Predicting with model: "+str(model.__class__))
            ns, ps = model.predict(batch)
            print("Prediction finished") 
            # TODO
            # Make sure both files have the same labels prior to sorting.

            names.append(ns)
            predictions.append(ps)

        return np.argmax(np.array(predictions) * self.weights)

# Weight frames higher/lower
# Weight models higher/lower depending on rate??
if __name__ == "__main__":

    dermo_location = "mat_dermo_code/"
    recod_location = "isbi2017-part3/"

    # TODO
    # Test,vali, train splits using sklearn..

    folder = "-".join(str(datetime.datetime.now()).split())

    derm_model = dermoNET(dermo_location, output_folder=folder)
    recod_model = RECOD(recod_location, output_folder=folder)
    ensemble = Ensemble(models=[recod_model, derm_model], weights=[0.65, 0.35])
    ensemble.predict()
