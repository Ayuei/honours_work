
# coding: utf-8

# In[1]:


import pandas as pd
import seaborn as sns
import sklearn.metrics as metrics
import numpy as np
import sys
import warnings
warnings.filterwarnings("ignore")
import matplotlib.pyplot as plt
import pickle
import re
from collections import defaultdict
from sklearn.metrics import *

sns.set_palette("coolwarm", 7)


# In[2]:


d_dict = defaultdict(lambda: [])
a = pickle.load(open("matched_files.pkl", 'rb'))

benign_case = defaultdict(lambda: [])
mel_case = defaultdict(lambda: [])

for k in a:
    new_key = a[k].split('/')[1]+"/"+a[k].split('/')[2]
    t = a[k].split('/')[-1]
    if 'mel' in k:
        mel_case[new_key].append(t)
    else:
        benign_case[new_key].append(t)
        
print(len(mel_case.keys()))
print(len(benign_case.keys()))


# In[3]:


reverse_dic = {}
for k, v in a.items():
    reverse_dic[v] = reverse_dic.get(v, [])
    reverse_dic[v].append(k)
    
assert list(filter(lambda k : len(reverse_dic[k]) != 1, reverse_dic)) == []


# In[4]:


def retrieveTimeFrame(img, baseline=False, new_key_retrieval=False, size=False):
    img = img+".jpg"
    #print(img)
    if 'mel' in img:
        n = int(re.match(r'(\d+){1}', img)[0])
        if n > 309:
            print(img)
            return None
        if baseline:
            accepted = list(range(0, 310, 10))
            #print(accepted)
            #print(n)
            if n not in accepted:
                return -1

        img = img.replace(str(n), str(n//10))

    new_key = a[img].split('/')[1]+"/"+a[img].split('/')[2]
    t = benign_case[new_key]
    t2 = mel_case[new_key]
    cases = benign_case[new_key] if "mel" not in img else mel_case[new_key]
    test = benign_case if "mel" not in img else mel_case
    num_data_points = len(cases)
    middle_point = num_data_points//2

    cases = sorted(cases)
    t = a[img].split('/')[-1]
    num = cases.index(t)
    #print(cases)
    #print(t, t2, img, new_key,num)
    if num_data_points == 0:
        print("ERROR HERE")
        raise Exception
    
    if new_key_retrieval and not size:
        return new_key, num
    if new_key_retrieval and size:
        return new_key, num, num_data_points
    return num/(num_data_points-1)


# In[5]:


int(False)


# In[6]:


def retrieveScores(img, num):

    recod_file = "baseline/isbi2017-rc36xtrm.txt_baseline"
    dermok_file = "baseline/predictions_baseline_softmax"
    scores = []
    flag = False 
    
    dictionary_scores = defaultdict(lambda : [])
    dictionary_labels = defaultdict(lambda : [])
    
    with open(recod_file, "r") as recod, open(dermok_file, "r") as dermo:
        for line in recod.readlines():
            try:
                f, score = line.strip().split(',')[0:2]
                # Mel, Serra
                score = float(score)
                dictionary_scores[f].append(score)
                dictionary_labels[f].append(int(score >= 0.5))
                flag = True
            except ValueError:
                print("Skipping: ", line.split())
                continue
                
        if not flag: raise Exception("Can't find img RECOD")

        for line in dermo.readlines():
            f, score1, score2 = line.split()
            score1 = float(score1); score2 = float(score2)
            f = f.replace(".jpg", "")
            flag = False
            # bengin, malig, serra
            
            #RESCALE
            score_new2 = score2/(score2+score1)
            score_new1 = score1/(score2+score1)
            
            dictionary_scores[f].append(score_new2)
            dictionary_labels[f].append(int(score2 >= score1))
            
        if flag: raise Exception("Can't find img DEMRO")

    return dictionary_scores, dictionary_labels

def unravel_dict(d):
    recod_labels = []
    dermo_labels = []
    trth = []
    
    for key in d:
        ls = d[key]
        recod_labels.append(ls[0])
        dermo_labels.append(ls[1])
        trth.append(int('mel' in key))
        
    return recod_labels, dermo_labels, trth

def calculate_metrics(confusion_matrix):
    print('INPUT:', confusion_matrix)
    tn, fn = confusion_matrix[0]
    fp, tp = confusion_matrix[1]

    prec = tp/(tp+fp)
    recall = tp/(tp+fn)

    f1 = 2/(1/prec + 1/recall)

    sensitivity =tp/(tp+fn)
    specifity = tn/(tn+fp) 

    acc = (tp+tn)/(tp+tn+fp+fn)

    print("Sensivity", sensitivity, specifity, f1, acc)
    print("Specificity", specifity)
    print("f1", f1)
    print("acc", acc)
    print([tp, fp])
    print([fn, tn])
    
    return sensitivity, specifity, acc

def getWeightedf1Diagnosistic(d, i=0):
    d = unravel_dict(d)
    #print('Hello', d[i], d[2])
    #print('confusion')
    
    sen, spec, acc = calculate_metrics(confusion_matrix(d[i], d[2], labels=[0,1]))
    return 2/((1/sen)+(1/spec))

def getWeightedf1DiagnosisticD(d, i=1):
    d = unravel_dict(d)
    #print('Hello', d[i], d[2])
    #print('confusion')
    #print(confusion_matrix(d[i], d[2]))
    sen, spec, acc = calculate_metrics(confusion_matrix(d[i], d[2], labels=[0,1]))
    return 2/((1/sen)+(1/spec))


# In[7]:


d_dict = defaultdict(lambda: [])
#match_names = pickle.load(open("matched_files.pkl", 'rb'))

benign_case = defaultdict(lambda: [])
mel_case = defaultdict(lambda: [])

for k in a:
    new_key = a[k].split('/')[1]+"/"+a[k].split('/')[2]
    t = a[k].split('/')[-1]
    if 'mel' in k:
        mel_case[new_key].append(t)
    else:
        benign_case[new_key].append(t)


# In[8]:


mel_case


# In[9]:


scores, labels = retrieveScores(None, None)

assert list(filter(lambda k : len(scores[k]) != 2, scores)) == []
assert len(labels) == 3608


assert list(filter(lambda k : len(labels[k]) != 2, labels)) == []
assert len(labels) == 3608


# In[10]:


#labels

print(calculate_metrics(confusion_matrix(unravel_dict(labels)[0], unravel_dict(labels)[2])))
print(calculate_metrics(confusion_matrix(unravel_dict(labels)[1], unravel_dict(labels)[2])))


# In[11]:


from copy import deepcopy
early_dict = defaultdict(lambda : [])
early_dict_scores = defaultdict(lambda : [])
middle_dict = defaultdict(lambda : [])
middle_dict_scores = defaultdict(lambda : [])
late_dict = defaultdict(lambda : [])
late_dict_scores = defaultdict(lambda : [])
candidates_dict = defaultdict(lambda : [])

#NOT USED FOR CALCULATION.

# [1, 0, 0], RECOD GOT IT WRONG! SUPRISINGINLY
single_time_point = defaultdict(lambda: [])

# Data distribution of times frames and how hard it was to diagnose for system (length)

#print(sorted(labels))

for key in sorted(labels):
    
    if labels[key][0] != labels[key][1]:
        candidates_dict[key] = deepcopy(labels[key])
    try:
        time_frame_data = retrieveTimeFrame(key, True)
    except ZeroDivisionError:
        single_time_point[key].extend(labels[key])
        continue
        
    if time_frame_data == None:
        continue
    
    if time_frame_data == 1.0:
        late_dict[key].extend(labels[key])
        late_dict_scores[key].extend(scores[key])
    elif time_frame_data == 0.0:
        early_dict[key].extend(labels[key])
        early_dict_scores[key].extend(scores[key])
    elif 0.0 < time_frame_data < 1.0:
        middle_dict[key].extend(labels[key])
        middle_dict_scores[key].extend(scores[key])
    else:
        print("PASSED", key)
        continue


# In[12]:


lengths = list(map(lambda k : len(k), [early_dict, middle_dict, late_dict, single_time_point]))
print(lengths)
print(sum(lengths), len(list(filter(lambda k: "mel" not in k, labels)))+31)
print(len(sorted(sorted(filter(lambda k : 'mel' in k, early_dict.keys()))+ sorted(filter(lambda k : 'mel' in k, late_dict.keys()))+sorted(filter(lambda k : 'mel' in k, middle_dict.keys())), key = lambda k : int(re.match(r'(\d+){1}', k)[0]))))
set(list(filter(lambda k: "mel" not in k, list(early_dict)+list(middle_dict)+list(late_dict)+list(single_time_point))))-set(list(filter(lambda k: "mel" not in k, labels)))


# In[13]:


print(len(candidates_dict))


# In[14]:


# RECOD, DERMO, TRTH

early = unravel_dict(early_dict)
late = unravel_dict(late_dict)
candidates = unravel_dict(candidates_dict)
middle = unravel_dict(middle_dict)
single = unravel_dict(single_time_point)

sanity = 0

counts_r = [0,0,0]
counts_d = [0,0,0]
for r1, d1, t1, r3, d3, t3 in zip(early[0], early[1], early[2], late[0], late[1], late[2]):
    if r1 == t1:
        counts_r[0] += 1
    if d1 == t1:
        counts_d[0] += 1

    if r3 == t3:
        counts_r[2] += 1
    if d3 == t3:
        counts_d[2] += 1
        
        
for r1, d1, t1 in zip(middle[0], middle[1], middle[2]):
    if r1 == t1:
        counts_r[1] += 1
    if d1 == t1:
        counts_d[1] += 1

print(counts_r, counts_d)
print(len(early[0]), len(middle[0]), len(late[0]))

# First Frame Acc, Middle Frame Acc, Last Frame Acc
data = list(map(getWeightedf1Diagnosistic, [early_dict, middle_dict, late_dict]))
print('DATA', data)

#ax = sns.barplot(x=["first", "middle", "last"], y=[])
plt.figure(figsize=(25,20))
ax = sns.barplot(x=["First", "Middle", "Last"], y=data)

ax.set_xlabel("Time Frame", fontsize=30, weight='light')
ax.set_ylabel("F-Measure", fontsize=30, weight='light')
ax.tick_params(labelsize=25)
#ax.set(yticks=np.arange, 1)
axes = ax.axes

#axes.set_ylim(,)

print(axes)

print()

plt.show()

ax.get_figure().savefig('RECOD_fold_baseline.png')


# In[15]:


# First Frame Acc, Middle Frame Acc, Last Frame Acc

data = list(map(getWeightedf1Diagnosistic, [early_dict, middle_dict, late_dict]))
print('DATA', data)

#ax = sns.barplot(x=["first", "middle", "last"], y=[])
plt.figure(figsize=(25,20))
ax = sns.barplot(x=["First", "Middle", "Last"], y=data)

ax.set_xlabel("Time Frame", fontsize=30, weight='light')
ax.set_ylabel("F-Measure", fontsize=30, weight='light')
ax.tick_params(labelsize=25)
axes = ax.axes

plt.show()
ax.get_figure().savefig('DERMO_fold_baseline.png')


# In[16]:


print(sum(early[2]))
print("RECOD")
calculate_metrics(confusion_matrix(early[0], early[2]))
print("DERMO")
calculate_metrics(confusion_matrix(early[1], early[2]))


# In[17]:


print(sum(middle[2]))
print("RECOD")
calculate_metrics(confusion_matrix(middle[0], middle[2]))
print("DERMO")
calculate_metrics(confusion_matrix(middle[1], middle[2]))


# In[18]:


print(sum(late[2]))
print("RECOD")
calculate_metrics(confusion_matrix(late[0], late[2]))
print("DERMO")
calculate_metrics(confusion_matrix(late[1], late[2]))


# In[19]:


from sklearn.svm import LinearSVC
from sklearn import preprocessing

def stratified_SVM(dat):
    from sklearn.model_selection import StratifiedKFold

    names, X, y = dat

    labels = [0 for i in range(len(names))]
    
    X = np.array(X)
    y = np.array(y)
    names_np = np.array(names)
    
    skf = StratifiedKFold(n_splits=6)

    total_y_true = []
    total_y_pred = []
    coefficents = []

    for train_index, test_index in skf.split(X, y):
        X_train, X_test = X[train_index], X[test_index]
        y_train, y_test = y[train_index], y[test_index]
        names_train, names_test = names_np[train_index], names_np[test_index]

        scaler = preprocessing.StandardScaler().fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)

        svm = LinearSVC()
        svm.fit(X_train, y_train)

        y_pred = svm.predict(X_test)
        total_y_true.extend(y_test)
        total_y_pred.extend(y_pred)
        
        for i, ind in enumerate(test_index):
            labels[ind] = y_pred[i]
        
        coefficents.extend(svm.coef_)

    print("SVC over 6 folds")
    print(np.mean(np.array(coefficents), axis=0))
    print(recall_score(total_y_true, total_y_pred), accuracy_score(total_y_true, total_y_pred))
    
    return names, labels


# In[20]:


svm_final = []

for i in range(8):
    print("REAL FOLD %s" % (i+1,))
    print("=========================")
    temp_r = []
    temp_d = []
    temp_g = []
    #names = []
    
    temp_r_scores=[]
    temp_d_scores=[]
    temp_g_scores=[]
    time_frame_fets=[]
    names_scores = []
    
    test = list(map(lambda k : k.replace('.jpg', ''), open("baseline/txts/test.txt%s" % (i,), "r").read().split(', ')))
    #print(test)
    for key in labels:
        if key in test:
            temp_r.append(labels[key][0])
            temp_d.append(labels[key][1])
            temp_g.append(int("mel" in key))
            #names.append(key)
            
    
    for key in scores:
        if key in test:
            temp_r_scores.append(scores[key][0])
            temp_d_scores.append(scores[key][1])
            temp_g_scores.append(int("mel" in key))
            time_frame_fets.append(retrieveTimeFrame(key))
            names_scores.append(key)
        
    assert len(temp_r) == len(temp_d) == len(temp_g) == len(test)
    assert len(temp_r_scores) == len(temp_d_scores) == len(temp_g_scores) == len(test) == len(time_frame_fets)
    
    #Errors if any nonetypes
    sum(time_frame_fets)
    
    print(sum(temp_g))
    print("RECOD")
    calculate_metrics(confusion_matrix(temp_r, temp_g))
    print("DERMO")
    calculate_metrics(confusion_matrix(temp_d, temp_g))
    
    X = np.array([[x,y,z] for x,y,z in zip(temp_r_scores, temp_d_scores, time_frame_fets)])
    
    svm_final.append(stratified_SVM([names_scores, X, temp_g_scores]))
    
    print("=========================")


# In[21]:


# differences_static_weights
score_r = 0; score_d = 0

for r, d, t in zip(candidates[0], candidates[1], candidates[2]):
    if r == t:
        score_r += 1
        
    if d == t:
        score_d += 1

print(score_r, score_d, len(candidates[2]))

# Harmonic mean of specificity and sensitivity

print("RATIO SHOULD BE: ", score_r/(score_r+score_d), score_d/(score_r+score_d))
time_weights_recod = [2/(1/0.8662420382165605 + 1/0.6666666666666666),2/(1/0.46153846153846156+1/0.8708860759493671)
                              ,2/(1/0.6666666666666666 + 1/0.89171974522293)]

time_weights_dermo = [2/(1/0.6666666666666666+1/0.7707006369426752), 2/(1/0.6153846153846154+1/0.7772151898734178
), 2/(1/0.7777777777777778+1/0.7770700636942676)]

# Normalise
time_weights_recod = [x/sum(time_weights_recod) for x in time_weights_recod]
time_weights_dermo = [x/sum(time_weights_dermo) for x in time_weights_dermo]

print("Time Frame Weights: ", time_weights_recod)
print("Time Frame Weights DERMO:", time_weights_dermo)

# CALCULATING THE SPATIAL AND TEMPORAL WEIGHTS VIA DIFFERENCE
# Validation set desirable but involves splitting up an already small dataset


# In[22]:


#RECOD
# MEL Serra
# DERMO
# bengin, malig, serra
# Combine malig

final_patients=defaultdict(lambda: [0 for i in range(10)])
final_patients_baseline = defaultdict(lambda: [0 for i in range(10)])

for e_key, l_key in zip(early_dict_scores, late_dict_scores):
    
    new_e_key, _ = retrieveTimeFrame(e_key, False, True)
    new_l_key, num = retrieveTimeFrame(l_key, False, True)
    
    final_patients[new_e_key][0] = early_dict_scores[e_key][0]*time_weights_recod[0]
    final_patients[new_l_key][num] = late_dict_scores[l_key][0]*time_weights_recod[2]
    
    final_patients_baseline[new_e_key][0] = early_dict_scores[e_key][0]*(1/3)
    final_patients_baseline[new_l_key][num] = late_dict_scores[l_key][0]*(1/3)

for m_key in middle_dict_scores:
    new_m_key, num = retrieveTimeFrame(m_key, False, True)
    
    final_patients[new_m_key][num] = middle_dict_scores[m_key][0]*time_weights_recod[1]
    final_patients_baseline[new_m_key][num] = middle_dict_scores[m_key][0]*(1/3)
    
final_preds = []
final_preds_baseline = []
grd_trth = []
for patient in final_patients:
    
    if len(mel_case[patient]) > 0:
        if(len(benign_case[patient]) > 0):
            raise Exception
        grd_trth.append(1)
    else:
        grd_trth.append(0)

    final_preds.append(int(sum(final_patients[patient]) >= 0.5))
    final_preds_baseline.append(int(sum(final_patients_baseline[patient]) >= 0.5))
    

print("BEFORE")
calculate_metrics(confusion_matrix(final_preds_baseline, grd_trth))
print("AFTER")
calculate_metrics(confusion_matrix(final_preds, grd_trth))

#print(early_dict_scores)
#late_dict_scores
#middle_dict_scores

# BEORE AND AFTER TEMPORAL WEIGHTING


# In[23]:


time_frame_fets = []
temp_r_scores = []
temp_d_scores = []
temp_g_scores = []
names = []

for key in scores:
    try:
        time_frame = retrieveTimeFrame(key)
        if time_frame is None:
            continue
        time_frame_fets.append(time_frame)
    except ZeroDivisionError:
        print("Should only happen once")
        continue
    
    temp_r_scores.append(scores[key][0])
    temp_d_scores.append(scores[key][1])
    temp_g_scores.append(int("mel" in key))
    names.append(key)
    
X = np.array([[x,y,z] for x,y,z in zip(temp_r_scores, temp_d_scores, time_frame_fets)])
stratified_SVM([names, X, temp_g_scores]) 


# In[25]:


#RECOD
# MEL Serra
# DERMO
# bengin, malig, serra
# Combine malig

final_patients=defaultdict(lambda: [0 for i in range(10)])
final_patients_baseline = defaultdict(lambda: [0 for i in range(10)])

for e_key, l_key in zip(early_dict_scores, late_dict_scores):
    
    new_e_key, _ = retrieveTimeFrame(e_key, False, True)
    new_l_key, num = retrieveTimeFrame(l_key, False, True)
    
    final_patients[new_e_key][0] = early_dict_scores[e_key][1]*time_weights_dermo[0]
    final_patients[new_l_key][num] = late_dict_scores[l_key][1]*time_weights_dermo[2]
    
    final_patients_baseline[new_e_key][0] = early_dict_scores[e_key][1]*(1/3)
    final_patients_baseline[new_l_key][num] = late_dict_scores[l_key][1]*(1/3)
    

for m_key in middle_dict_scores:
    new_m_key, num = retrieveTimeFrame(m_key, False, True)
    
    final_patients[new_m_key][num] = middle_dict_scores[m_key][1]*time_weights_dermo[1]
    final_patients_baseline[new_m_key][num] = middle_dict_scores[m_key][1]*(1/3)
    
final_preds = []
final_preds_baseline = []
grd_trth = []
for patient in final_patients:
    
    if len(mel_case[patient]) > 0:
        if(len(benign_case[patient]) > 0):
             Exception
        grd_trth.append(1)
    else:
        grd_trth.append(0)

    final_preds.append(int(sum(final_patients[patient]) >= 0.5))
    final_preds_baseline.append(int(sum(final_patients_baseline[patient]) >= 0.5))
    

print("BEFORE")
calculate_metrics(confusion_matrix(final_preds_baseline, grd_trth))
print("AFTER")
calculate_metrics(confusion_matrix(final_preds, grd_trth))

#print(early_dict_scores)
#late_dict_scores
#middle_dict_scores


# BEORE AND AFTER TEMPORAL WEIGHTING


# In[ ]:


# DERMO DOESN'T CHANGE MUCH since it's very close to equality (1/3)


# In[87]:


def retrieveScores(img, num):

    recod_file = "/home/vin/svm/isbi2017-rc36xtrm.txt%s" %(num,)
    dermok_file = "/home/vin/svm/predictions_%s" %(num,)
    scores = []
    flag = False 
    
    dictionary_scores = defaultdict(lambda : [])
    dictionary_labels = defaultdict(lambda : [])
    
    with open(recod_file, "r") as recod, open(dermok_file, "r") as dermo:
        for line in recod.readlines():
            try:
                f, score = line.strip().split(',')[0:2]
                # Mel, Serra
                score = float(score)
                dictionary_scores[f].append(score)
                dictionary_labels[f].append(int(score >= 0.5))
                flag = True
            except ValueError:
                #print("Skipping: ", line.split())
                continue
                
        if not flag: raise Exception("Can't find img RECOD")

        for line in dermo.readlines():
            f, score1, score2 = line.split()
            score1 = float(score1); score2 = float(score2)
            f = f.replace(".jpg", "")
            flag = False
            # bengin, malig, serra
            
            #RESCALE
            score_new2 = score2/(score2+score1)
            score_new1 = score1/(score2+score1)
            
            dictionary_scores[f].append(score_new2)
            dictionary_labels[f].append(int(score2 >= score1))
            
        if flag: raise Exception("Can't find img DEMRO")

    return dictionary_scores, dictionary_labels


dermo_graph_b = []
dermo_graph_a = []
recod_graph_b = []
recod_graph_a  = []
svm_final = []

for i in range(8):
    print("FOLD", i+1)
    print("+++++++++++++++++++")
    
    scores, labels = retrieveScores(None, i)
    
    early_dict = defaultdict(lambda : [])
    early_dict_scores = defaultdict(lambda : [])
    middle_dict = defaultdict(lambda : [])
    middle_dict_scores = defaultdict(lambda : [])
    late_dict = defaultdict(lambda : [])
    late_dict_scores = defaultdict(lambda : [])
    candidates_dict = defaultdict(lambda : [])

    #NOT USED FOR CALCULATION.

    # [1, 0, 0], RECOD GOT IT WRONG! SUPRISINGINLY
    single_time_point = defaultdict(lambda: [])

    # Data distribution of times frames and how hard it was to diagnose for system (length)

    #print(sorted(labels))

    for key in sorted(labels):

        if labels[key][0] != labels[key][1]:
            candidates_dict[key] = deepcopy(labels[key])
        try:
            time_frame_data = retrieveTimeFrame(key, False)
            #print(time_frame_data)
        except ZeroDivisionError:
            single_time_point[key].extend(labels[key])
            continue

        if time_frame_data == None:
            continue

        if time_frame_data == 1.0:
            late_dict[key].extend(labels[key])
            late_dict_scores[key].extend(scores[key])
        elif time_frame_data == 0.0:
            early_dict[key].extend(labels[key])
            early_dict_scores[key].extend(scores[key])
        elif 0.0 < time_frame_data < 1.0:
            middle_dict[key].extend(labels[key])
            middle_dict_scores[key].extend(scores[key])
        else:
            #print("PASSED", key)
            continue
                 
    #RECOD
    # MEL Serra
    # DERMO
    # bengin, malig, serra
    # Combine malig
    
    if(i == 1):
        pass
        
    
    # What I did to future me
    # I took all the replicas put them into seperate lists and treated them like seperate patients
    # For the SVM features [early_red, AVERAGE of middle_recod, late_recod] [early_dermo,  AVERAGE of middle_dermo etc]
    # if there's no middle, input 0.
    final_patients=defaultdict(lambda: [[0 for i in range(100)] for i in range(10)])
    final_patients_baseline = defaultdict(lambda: [[0 for i in range(100)] for i in range(10)])
    # Do this to collect the datas, and then unravel!
    svm_feats = defaultdict(lambda : [([0, [0], 0], 
                                       [0, [0], 0]) for i in range(10)])

    for e_key, l_key in zip(early_dict_scores, late_dict_scores):
        try:
            replica_index_e = 0; replica_index_l = 0

            if 'mel' in e_key:
                replica_index_e = int(re.match(r'(\d+){1}', e_key)[0][-1])

            if 'mel' in l_key:
                replica_index_l = int(re.match(r'(\d+){1}', l_key)[0][-1])
            
            new_e_key, _ = retrieveTimeFrame(e_key, False, True)
            new_l_key, num = retrieveTimeFrame(l_key, False, True)
            
            final_patients[new_e_key][replica_index_e][0] = (early_dict_scores[e_key][0]*time_weights_recod[0])
            final_patients[new_l_key][replica_index_l][num] = (late_dict_scores[l_key][0]*time_weights_recod[2])
            
            svm_feats[new_e_key][replica_index_e][0][0] = (early_dict_scores[e_key][0])
            svm_feats[new_l_key][replica_index_l][0][2] = (late_dict_scores[l_key][0])
            
            final_patients_baseline[new_e_key][replica_index_e][0] = (early_dict_scores[e_key][0]*(1/3))
            final_patients_baseline[new_l_key][replica_index_l][num] = (late_dict_scores[l_key][0]*(1/3))
        
        except TypeError:
            continue

    for m_key in middle_dict_scores:
        try:
            new_m_key, num, length = retrieveTimeFrame(m_key, False, True, True)
            
            replica_index_m = 0

            if 'mel' in e_key:
                replica_index_m = int(re.match(r'(\d+){1}', m_key)[0][-1])
            
            final_patients[new_m_key][replica_index_m][num] = (middle_dict_scores[m_key][0]*time_weights_recod[1]/length)
            final_patients_baseline[new_m_key][replica_index_m][num] = (middle_dict_scores[m_key][0]*(1/(3*length)))
            svm_feats[new_m_key][replica_index_m][0][1].append(middle_dict_scores[m_key][0])
            
        except TypeError:
            continue

    final_preds = []
    final_preds_baseline = []
    grd_trth = []
    for patient in final_patients:

        if len(mel_case[patient]) > 0:
            if(len(benign_case[patient]) > 0):
                raise
            grd_trth.extend([1 for i in range(10)])
        else:
            grd_trth.extend([0 for i in range(10)])
                                                  
        for i, ele in enumerate(final_patients[patient]):        
            ele = sum(ele)
            final_patients[patient][i] = int(ele>=0.5)
            
        for i, ele in enumerate(final_patients_baseline[patient]):
            ele = sum(ele)
            final_patients_baseline[patient][i] = int(ele >= 0.5)
        
        final_preds.extend(final_patients[patient])
        final_preds_baseline.extend(final_patients_baseline[patient])

    print("RECOD")
    print("BEFORE")
    sensitivity, specifity, acc=calculate_metrics(confusion_matrix(final_preds_baseline, grd_trth))
    recod_graph_b.append([sensitivity, specifity, acc])
    print("AFTER")
    sensitivity, specifity, acc=calculate_metrics(confusion_matrix(final_preds, grd_trth))
    recod_graph_a.append([sensitivity, specifity, acc])
    
    #DERMO
    # MEL Serra
    # DERMO
    # bengin, malig, serra
    # Combine malig

    final_patients=defaultdict(lambda: [[0 for i in range(100)] for i in range(10)])
    final_patients_baseline = defaultdict(lambda: [[0 for i in range(100)] for i in range(10)])

    for e_key, l_key in zip(early_dict_scores, late_dict_scores):
        try:
            replica_index_e = 0; replica_index_l = 0

            if 'mel' in e_key:
                replica_index_e = int(re.match(r'(\d+){1}', e_key)[0][-1])

            if 'mel' in l_key:
                replica_index_l = int(re.match(r'(\d+){1}', l_key)[0][-1])
            
            new_e_key, _ = retrieveTimeFrame(e_key, False, True)
            new_l_key, num = retrieveTimeFrame(l_key, False, True)
            
            final_patients[new_e_key][replica_index_e][0] = (early_dict_scores[e_key][1]*time_weights_dermo[0])
            final_patients[new_l_key][replica_index_l][num] = (late_dict_scores[l_key][1]*time_weights_dermo[2])
            
            svm_feats[new_e_key][replica_index_e][1][0] = (early_dict_scores[e_key][1])
            svm_feats[new_l_key][replica_index_l][1][2] = (late_dict_scores[l_key][1])

            final_patients_baseline[new_e_key][replica_index_e][0] = (early_dict_scores[e_key][1]*(1/3))
            final_patients_baseline[new_l_key][replica_index_l][num] = (late_dict_scores[l_key][1]*(1/3))
        
        except TypeError:
            continue

    for m_key in middle_dict_scores:
        try:
            new_m_key, num, length = retrieveTimeFrame(m_key, False, True, True)
            
            replica_index_m = 0

            if 'mel' in e_key:
                replica_index_m = int(re.match(r'(\d+){1}', m_key)[0][-1])
            
            final_patients[new_m_key][replica_index_m][num] = (middle_dict_scores[m_key][1]*time_weights_recod[1]/length)
            final_patients_baseline[new_m_key][replica_index_m][num] = (middle_dict_scores[m_key][1]*(1/(3*length)))
            svm_feats[new_m_key][replica_index_m][1][1].append(middle_dict_scores[m_key][1])
        except TypeError:
            continue

    final_preds = []
    final_preds_baseline = []
    grd_trth = []
    for patient in final_patients:

        if len(mel_case[patient]) > 0:
            if(len(benign_case[patient]) > 0):
                raise
            grd_trth.extend([1 for i in range(10)])
        else:
            grd_trth.extend([0 for i in range(10)])
                                                  
        for i, ele in enumerate(final_patients[patient]):
            ele = sum(ele)
            final_patients[patient][i] = int(ele>=0.5)
            
        for i, ele in enumerate(final_patients_baseline[patient]):
            ele = sum(ele)
            final_patients_baseline[patient][i] = int(ele >= 0.5)
        
        final_preds.extend(final_patients[patient])
        final_preds_baseline.extend(final_patients_baseline[patient])
    
    svm_final.append(svm_feats)
    
    print("DERMO")
    print("BEFORE")
    sensitivity, specifity, acc = calculate_metrics(confusion_matrix(final_preds_baseline, grd_trth))
    dermo_graph_b.append([sensitivity, specifity, acc])
    print("AFTER")
    if i+1 == 5:
        print("START FOLD 5 calc")
    sensitivity1, specifity1, acc = calculate_metrics(confusion_matrix(final_preds, grd_trth))
    dermo_graph_a.append([sensitivity1, specifity1, acc])
    
    print("+++++++++++++++++++")
    
    # RECOD, DERMO, TRTH

    early = unravel_dict(early_dict)
    late = unravel_dict(late_dict)
    candidates = unravel_dict(candidates_dict)
    middle = unravel_dict(middle_dict)
    single = unravel_dict(single_time_point)

    sanity = 0

    counts_r = [0,0,0]
    counts_d = [0,0,0]
    for r1, d1, t1, r3, d3, t3 in zip(early[0], early[1], early[2], late[0], late[1], late[2]):
        if r1 == t1:
            counts_r[0] += 1
        if d1 == t1:
            counts_d[0] += 1

        if r3 == t3:
            counts_r[2] += 1
        if d3 == t3:
            counts_d[2] += 1

    for r1, d1, t1 in zip(middle[0], middle[1], middle[2]):
        if r1 == t1:
            counts_r[1] += 1
        if d1 == t1:
            counts_d[1] += 1

    print(counts_r, counts_d)
    print(len(early[0]), len(middle[0]), len(late[0]))
    print("DATA CALCULATION")
    # First Frame Acc, Middle Frame Acc, Last Frame Acc
    data = list(map(getWeightedf1Diagnosistic, [early_dict, middle_dict, late_dict]))
    print("DATA END CALCULATION")
    #ax = sns.barplot(x=["first", "middle", "last"], y=[])
    plt.figure(figsize=(25,20))
    ax = sns.barplot(x=["First", "Middle", "Last"], y=data)

    ax.set_xlabel("Time Frame", fontsize=30, weight='light')
    ax.set_ylabel("F-Measure", fontsize=30, weight='light')
    ax.tick_params(labelsize=25)
    ax.set(yticks=np.arange(0, 1, 0.1))
    axes = ax.axes

    plt.show()
    print("RECOD", data)
    print("DATA CALCULATION")
    ax.get_figure().savefig('RECOD_fold_%s.png' %(i+1,))
    data = list(map(getWeightedf1DiagnosisticD, [early_dict, middle_dict, late_dict]))
    print("DATA END CALCULATION")
    #ax = sns.barplot(x=["first", "middle", "last"], y=[])
    plt.figure(figsize=(25,20))
    ax = sns.barplot(x=["First", "Middle", "Last"], y=data)

    ax.set_xlabel("Time Frame", fontsize=30, weight='light')
    ax.set_ylabel("F-Measure", fontsize=30, weight='light')
    ax.tick_params(labelsize=25)
    ax.set(yticks=np.arange(0, 1, 0.1))
    print("DERMO", data)
    axes = ax.axes
    
    plt.show()
    
    ax.get_figure().savefig('DERMO_fold_%s.png' %(i+1,))


# In[86]:


for i in range(len(svm_final)):
    for key in svm_final[i]:
        for ele in svm_final[i][key]:
            svm_final[i][key] = list(filter(lambda k: k != ([0, [0], 0], [0, [0], 0]), svm_final[i][key]))

print(svm_final[1])


# In[33]:


dermo_graph_a


# In[34]:


recod_graph_a


# In[35]:


dermo_graph_b


# In[36]:


recod_graph_b


# In[37]:


import pandas as pd

def get_quick_and_dirty_f_measure(s):
    s = np.array(s)
    return 2/(1/s[:,0]+1/s[:,1])

df= pd.DataFrame({"Baseline":get_quick_and_dirty_f_measure(recod_graph_b),
                 "Temporally Weighted":get_quick_and_dirty_f_measure(recod_graph_a)})
df


# In[38]:


sns_plot = sns.lineplot(data=df)
sns_plot.set(xlabel="Fold No.", ylabel="F-Measure")


# In[39]:


df= pd.DataFrame({"Baseline":get_quick_and_dirty_f_measure(dermo_graph_b),
                 "Temporally Weighted":get_quick_and_dirty_f_measure(dermo_graph_a)})
df


# In[40]:


sns_plot = sns.lineplot(data=df)
sns_plot.set(xlabel="Fold No.", ylabel="F-Measure")


# In[41]:


unfolded = []

import time

for fold, labels in svm_final:
    final_patient = defaultdict(lambda: [0 for i in range(100)])
    final_preds = defaultdict(lambda: [0,0])
    counter_dict = defaultdict(lambda: 0)
    for img, label in zip(fold, labels):
        new_key, num = retrieveTimeFrame(img, False, True)
        final_patient[new_key][num] = label
        counter_dict[new_key]+=1
    
    for key in counter_dict:
        items = final_patient[key]
        counts_mel = 0
        counts_ben = 0
        for i in range(counter_dict[key]):
            if items[i] == 1:
                counts_mel+=1
            else:
                counts_ben+=1
                
        final_preds[key][0] = int(counts_mel > counts_ben)    
        final_preds[key][1] = 1 if len(mel_case[key]) > 0 else 0
    unfolded.append(final_preds)
print(unfolded[5])


# In[42]:


def get_quick_and_dirty_acc(s):
    s = np.array(s)
    
    return s[:,2]
    

df= pd.DataFrame({"Baseline":get_quick_and_dirty_acc(recod_graph_b),
                 "Temporally Weighted":get_quick_and_dirty_acc(recod_graph_a)})
df


# In[43]:


sns_plot = sns.lineplot(data=df)
sns_plot.set(xlabel="Fold No.", ylabel="Accuracy")


# In[45]:


def get_quick_and_dirty_acc(s):
    s = np.array(s)
    
    return s[:,2]
    

df= pd.DataFrame({"Baseline":get_quick_and_dirty_acc(dermo_graph_b),
                 "Temporally Weighted":get_quick_and_dirty_acc(dermo_graph_a)})
df


# In[46]:


sns_plot = sns.lineplot(data=df)
sns_plot.set(xlabel="Fold No.", ylabel="Accuracy")


# In[ ]:


svm_final


# In[ ]:


df= pd.DataFrame({"Baseline":get_quick_and_dirty_acc(dermo_graph_b),
                 "Temporally Weighted":get_quick_and_dirty_acc(dermo_graph_a)})
df


# In[ ]:


sns_plot = sns.lineplot(data=df)
sns_plot.set(xlabel="Fold No.", ylabel="Accuracy")

