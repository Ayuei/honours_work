import sklearn.metrics as metrics
import numpy as np
import sys
import warnings
warnings.filterwarnings("ignore")
with open(sys.argv[1]) as data:
	data = data.readlines()
	
	print(data[0])
	pred_labels = [np.argmax(map(float, line.split(',')[1:])).astype(float) for line in data]
	
	grd_trth = [0 if 'benign' in line.split(',')[0] else 1 for line in data]

	print("Acc: "+ str(metrics.accuracy_score(grd_trth, pred_labels)))
	print("Confusion Matrix:")
	print(metrics.confusion_matrix(grd_trth, pred_labels))
