from sklearn.metrics import *
import sys

trth = []
probs = []
preds = []
for line in open(sys.argv[1], "r").readlines():
    line=line.strip()
    try:
        preds.append(1 if float(line.split(",")[1]) > 0.50 else 0)
        trth.append(1 if 'mel' in line else 0)
        probs.append(line.split(",")[1])
    except IndexError:
        break
#print(probs[labels.index(1)])
#print(labels.index(1))
#print(accuracy_score(trth, labels))
# Inversions?
#for i, zipped in enumerate(zip(preds, labels)):
#	pred, label = zipped
#	if pred != label:
#		print(pred, label)

grds = trth
print(accuracy_score(grds, preds))
print(confusion_matrix(grds, preds))
cm = confusion_matrix(grds,preds)
print("F1",f1_score(grds, preds))
tp = cm[1][1]
fp = cm[0][1]
fn = cm[1][0]
tn = cm[0][0]

print("Specificity", tn/(tn+fp))
print("Sensivity", tp/(tp+fn))
#
#    [TP FP] Specificity = tn/tn+fp
#    [FN TN] Sensitivity = tp/tp+fn   
