
# coding: utf-8

# In[1]:

import seaborn as sns
from collections import defaultdict
import re
import matplotlib.pyplot as plt
import sklearn.metrics as metrics


# In[2]:


d_dict = defaultdict(lambda: [])

with open("predictions.txt", "r") as f:
    for line in f.readlines()[1:]:
        m = None
        try:
            m = re.findall(r"\d{3}", line)[-1]
        except IndexError:
            continue
        try:
            vals = list(map(int, line.split()[-2:]))
        except ValueError as e:
            print(e)
            continue
        vals.insert(0, int(m))

        d_dict["/".join(line.strip().split("/")[0:-1])].append(vals)
for key in d_dict:
    d_dict[key].sort()


# In[3]:


first_image = 0
last_image = 0

first_image_pred = []
last_image_pred = []
first_image_trth = []
last_image_trth = []

total = 0

for key in d_dict:
    lists = d_dict[key]
    
    #Indexes 
    # 1 = Pred #2 = Grth Trth
    
    #if len(lists) <= 2:
        #print("Too few?", key, "len", len(lists))
    
    # If image is not Melanoma we skip it
    #if lists[0][2] != 1:
    #    continue
    
    if lists[0][1] == lists[0][2]:
        first_image += 1
    
    if lists[-1][1] == lists[-1][2]:
        last_image += 1
    
    first_image_pred.append(lists[0][1])
    last_image_pred.append(lists[-1][1])
    first_image_trth.append(lists[0][2])
    last_image_trth.append(lists[-1][2])
    
    total += 1
    
print(d_dict[key])


# In[4]:


print(first_image, last_image, total)
#print(len(d_dict))


# In[5]:


data = [first_image/total, last_image/total]

sns.barplot(x=["first", "last"], y=data)


# In[6]:


plt.tight_layout(h_pad=2)


# In[7]:


plt.show()


# In[8]:


print('First frame acc overall', metrics.accuracy_score(first_image_trth, first_image_pred))
print('Last frame acc overall', metrics.accuracy_score(last_image_trth, last_image_pred))


# In[9]:


print('First frame acc confusion matrix')
print(metrics.confusion_matrix(first_image_trth, first_image_pred))
# Confusion Matrix
# [ benign_T, benign_F ]
# [ mel_F, mel_T]


# In[10]:


print(metrics.confusion_matrix(last_image_trth, last_image_pred))

