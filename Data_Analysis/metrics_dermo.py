from sklearn.metrics import *

import sys

if len(sys.argv) < 2:
    print("USAGE python metrics.py <file_name>")
    sys.exit(1)

with open(sys.argv[1], "r") as f:
    preds = []; grds = []
   
    for line in f.readlines()[1:]:
        data = line.split()
        pre = int(data[-2])
        grd= int(data[-1])
        preds.append(pre)
        grds.append(grd)
    print(grds)
    print(preds)
    print(accuracy_score(grds, preds))
    print(confusion_matrix(grds, preds))
    cm = confusion_matrix(grds,preds)
    print("F1",f1_score(grds, preds))
    tp = cm[1][1]
    fp = cm[0][1]
    fn = cm[1][0]
    tn = cm[0][0]

    print("Specificity", tn/(tn+fp))
    print("Sensivity", tp/(tp+fn))
#
#    [TP FP] Specificity = tn/tn+fp
#    [FN TN] Sensitivity = tp/tp+fn   
