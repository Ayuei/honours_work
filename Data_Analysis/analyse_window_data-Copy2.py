
# coding: utf-8

# In[1]:


import pandas as pd
import seaborn as sns
import sklearn.metrics as metrics
import numpy as np
import sys
import warnings
warnings.filterwarnings("ignore")
import matplotlib.pyplot as plt
import pickle
import re

from collections import defaultdict


# In[2]:


d_dict = defaultdict(lambda: [])
match_names = pickle.load(open("matched_files.pkl", 'rb'))

#print(match_names.keys())

with open("copy_folder/isbi2017-rc36xtrm.txt") as f:
    for line in f.readlines()[1:]:
        
        line = line.replace(line.split(',')[0], match_names[line.split(',')[0]+'.jpg'])
        
        m = re.search(r"\d{3}", line)
        try:
            
            pred_label = np.argmax(list(map(float, line.split(',')[1:]))).astype(int)
            grd_trth = 0 if 'benign' in line.split(',')[0] else 1 
            vals = [pred_label, grd_trth]
            
        except ValueError as e:
            print(e)
            continue
        
        vals.insert(0, int(m.group(0)))

        d_dict["/".join(line.strip().split("/")[0:-1])].append(vals)
for key in d_dict:
    d_dict[key].sort()


# In[3]:


#print(d_dict.items())


# In[4]:


first_image = 0
last_image = 0

first_image_pred = []
last_image_pred = []
first_image_trth = []
last_image_trth = []

total = 0

for key in d_dict:
    lists = d_dict[key]
    
    #Indexes 
    # 1 = Pred #2 = Grth Trth
    
    #if len(lists) <= 2:
        #print("Too few?", key, "len", len(lists))
    
    # If image is not Melanoma we skip it
    #if lists[0][2] != 1:
    #    continue
    
    if lists[0][1] == lists[0][2]:
        first_image += 1
    
    if lists[-1][1] == lists[-1][2]:
        last_image += 1
    
    first_image_pred.append(lists[0][1])
    last_image_pred.append(lists[0][2])
    first_image_trth.append(lists[-1][1])
    last_image_trth.append(lists[-1][2])
    
    total += 1
    
print(d_dict[key])


# In[5]:


print(first_image, last_image, total)
print(len(d_dict))


# In[6]:


data = [first_image/total, last_image/total]

sns.barplot(x=["first", "last"], y=data)


# In[7]:


plt.tight_layout(h_pad=2)


# In[8]:


plt.show()


# In[9]:


print('First frame acc overall', metrics.accuracy_score(first_image_trth, first_image_pred))
print('Last frame acc overall', metrics.accuracy_score(last_image_trth, last_image_pred))


# In[10]:


print('First frame acc confusion matrix')
print(metrics.confusion_matrix(first_image_trth, first_image_pred))
# Confusion Matrix
# [ benign_T, benign_F ]
# [ mel_F, mel_T]


# In[11]:


print(metrics.confusion_matrix(last_image_trth, last_image_pred))

