import numpy as np
from sklearn.model_selection import train_test_split
import glob
from random import shuffle
from shutil import copyfile

malignant_count = 0
benign_count = 0

data = glob.glob(merged_data/'*.jpg')

shuffle(data)

training_label = []

for datum in data:
    if 'mel' in datum:
        malignant_count += 1
        training_label.append(1)
    else:
        training_label.append(0)

benign_count = len(data)-malignant_count

print('Class Ratio:',benign_count,':',malignant_count)

X_train, X_test, y_train, y_test = train_test_split(
    data, training_label, test_size = 0.33, stratify=training_label
)

for f in X_train:
    copyfile(f, 'train/'+f)

for f in X_test:
    copyfile(f, 'test/'+f)

